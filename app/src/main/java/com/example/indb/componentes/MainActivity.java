package com.example.indb.componentes;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private static class ViewHolder{

        private Button mButtonToast;
        private Button mButtonSnack;
        private Spinner mSpinner;
        private Button mButtonProgress;
        private Button mButtonDate;
        private ProgressDialog mProgressDialog;
    }
    private ViewHolder mViewHolder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mViewHolder.mButtonToast = this.findViewById(R.id.button_toast);
        this.mViewHolder.mButtonSnack = this.findViewById(R.id.button_snackbar);
        this.mViewHolder.mSpinner = this.findViewById(R.id.spinner_dynamic);
        this.mViewHolder.mButtonProgress = this.findViewById(R.id.button_progress_dialog);
        this.mViewHolder.mButtonDate = this.findViewById(R.id.button_date);
        this.mViewHolder.mProgressDialog = new ProgressDialog(this);

        this.setListeners();
        this.loadSpinner();
    }

    private void loadSpinner() {
        List<String> lst = Mock.getWeightMock();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, lst);
        this.mViewHolder.mSpinner.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.button_toast) {
            Toast toast = Toast.makeText(this, R.string.toast, Toast.LENGTH_LONG);

            LayoutInflater inflater = getLayoutInflater();
            View toastLayout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.linear_toast));

            toast.setView(toastLayout);
            TextView text = toast.getView().findViewById(R.id.text_toast);

            //View view = toast.getView();
            //view.setBackgroundColor(Color.BLACK);

            text.setTextColor(Color.CYAN);
            toast.show();
        } else if (id == R.id.button_snackbar) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.constraint), R.string.snack, Snackbar.LENGTH_LONG);
            snackbar.setAction(R.string.undo_message, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), R.string.undo, Toast.LENGTH_SHORT).show();
                }
            });

            TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.BLUE);

            View view = snackbar.getView();
            view.setBackgroundColor(Color.LTGRAY);

            snackbar.show();
        } else if (id == R.id.button_progress_dialog) {
            this.mViewHolder.mProgressDialog.setTitle("titulo");
            this.mViewHolder.mProgressDialog.setMessage("Mensagem");
            this.mViewHolder.mProgressDialog.setTitle("titulo");
            this.mViewHolder.mProgressDialog.show();
        } else if (id == R.id.button_date) {
            this.showDatePickerDialog();
        }
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);


        new DatePickerDialog(this, null,  year, month, day).show();
    }

    private void setListeners() {
        this.mViewHolder.mButtonToast.setOnClickListener(this);
        this.mViewHolder.mButtonSnack.setOnClickListener(this);
        this.mViewHolder.mButtonProgress.setOnClickListener(this);
        this.mViewHolder.mButtonDate.setOnClickListener(this);

    }
}
